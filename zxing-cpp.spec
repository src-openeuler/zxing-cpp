Name: zxing-cpp
Version: 1.2.0
Release: 3
Summary: C++ port of the ZXing ("Zebra Crossing") barcode scanning library

License: LGPLv2 with exceptions or LGPLv3 with exceptions
Url: https://github.com/nu-book/zxing-cpp
Source0: %{url}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires: gcc-c++ cmake cmake(fmt)
BuildRequires: stb_image-devel stb_image-static
BuildRequires: stb_image_write-devel stb_image_write-static
# https://github.com/nu-book/zxing-cpp/issues/248
Patch0: 0001-Add-a-mode-to-build-against-system-versions-of-depen.patch
# Update stb_image/stb_image_write
# https://github.com/nu-book/zxing-cpp/pull/269
Patch1: 269.patch
# Use the system copy of pybind11 rather than trying to download a copy. This
# patch is unconditional, so it is not, as-is, suitable for sending upstream.
Patch2: zxing-cpp-1.2.0-system-pybind11.patch
# fix for recent libfmt
Patch3: 0001-test-update-to-libfmt-v9.0.0.patch
# fix for gcc 13
Patch4: 0001-include-cstdint.patch

%description
ZXing-C++ ("zebra crossing") is an open-source, multi-format 1D/2D barcode
image processing library implemented in C++.

%package devel
License: ASL 2.0 and (LGPLv2 with exceptions or LGPLv3 with exceptions)
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1
# remove bundled stb libraries:
rm -v thirdparty/stb/stb_image_write.h thirdparty/stb/stb_image.h
# stb_image.h is trivially forked: reconstruct the changes with the latest
# unbundled copy
cp -p /usr/include/stb/stb_image.h thirdparty/stb/
pushd thirdparty/stb
patch -p1 < stb_image.patch
popd

%build
%cmake -DBUILD_EXAMPLES=OFF -DBUILD_PYTHON_MODULE=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo
%cmake_build

%install
%cmake_install

%check

%files
%license LICENSE LICENSE.ZXing LICENSE.Qt LGPL_EXCEPTION.Qt NOTICE
%{_libdir}/libZXing.so.1
%{_libdir}/libZXing.so.%{version}

%files devel
%doc README.md
%{_includedir}/ZXing/
%{_libdir}/libZXing.so
%{_libdir}/cmake/ZXing/
%{_libdir}/pkgconfig/zxing.pc

%changelog
* Tue Feb 18 2025 peijiankang <peijiankang@kylinos.cn> - 1.2.0-3
- fix build error about build

* Wed Apr 10 2024 Eustace <eusteuc@outlook.com> - 1.2.0-2
- Dependency satisfaction

* Wed Sep 06 2023 Darssin <2020303249@mail.nwpu.edu.cn> - 1.2.0-1
- Package init
